# OpenMW 0.47.0 is still in RC-phase

Hello everyone!

The upcoming stable release of 0.47.0 is still in RC-phase. The project moves on steadily as usual, so don't worry about that! The reason for the delay is that we found an unusual amount of regressions and release-blocking bugs during this RC-phase. This really shows how important RC-phases are, to look at the bright side of it. We are currently waiting for the seventh iteration of release candidates, and hopefully this will be the last.

Stay tuned and thanks for sticking around!
